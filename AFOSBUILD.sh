
if [ $(uname -m | grep 'x86_64') ]; then
   cp -Rf geckodriver-amd64 /opt/ANDRAX/bin/geckodriver

   if [ $? -eq 0 ]
   then
     # Result is OK! Just continue...
     echo "Copy PACKAGE... PASS!"
   else
     # houston we have a problem
     exit 1
   fi
   else
      cp -Rf geckodriver-aarch64 /opt/ANDRAX/bin/geckodriver

      if [ $? -eq 0 ]
      then
        # Result is OK! Just continue...
        echo "Copy PACKAGE... PASS!"
      else
        # houston we have a problem
        exit 1
      fi
   fi

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
